  /**
   * 通用返回类型
   */
  export type BaseResponse<T> = {
    code: number,
    data: T,
    message: string,
    description: string,
  }