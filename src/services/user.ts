import myAxios from "../plugins/myAxios";
import { setCurrentUserState } from "../states/user";
import { BaseResponse } from "../models/typings";


export const getCurrentUser = async () => {
    // const currentUser = getCurrentUserState();
    // if (currentUser) {
    //     return currentUser;
    // }
    // 不存在则从远程获取
    const res:BaseResponse<any> = await myAxios.get('/user/current');
    if (res.code === 0) {
        setCurrentUserState(res.data);
        return res.data;
    }
    return null;
}

